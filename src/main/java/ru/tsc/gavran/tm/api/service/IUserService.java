package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User findByLogin(String login);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String id, String password);

    User setRole(String id, Role role);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User updateUserById(String id, String lastName, String firstName, String middleName, String email);

    User updateUserByLogin(String login, String lastName, String firstName, String middleName, String email);

}