package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, String projectId);

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

    Project removeProjectById(String userId, String projectId);

    Project removeProjectByIndex(String userId, int index);

    Project removeProjectByName(String userId, String name);

}
