package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, final E entity);

    void remove(String userId, final E entity);

    List<E> findAll(String userId);

    void clear(String userId);

    E findById(String userId, final String id);

    E removeById(String userId, final String id);

    E findByIndex(String userId, final Integer index);

    List<E> findAll(String userId, final Comparator<E> comparator);

    boolean existsById(String userId, final String id);

    E removeByIndex(String userId, final Integer index);

}
