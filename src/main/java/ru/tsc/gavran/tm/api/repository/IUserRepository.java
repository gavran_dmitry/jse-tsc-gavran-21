package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

}
