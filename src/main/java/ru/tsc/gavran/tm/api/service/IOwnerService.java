package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    E add(final String userId, final E entity);

    void remove(final String userId, final E entity);

    List<E> findAll(final String userId);

    void clear(final String userId);

    E findById(final String userId, final String id);

    E removeById(final String userId, final String id);

    E removeByIndex(final String userId, final Integer index);

    List<E> findAll(final String userId, final Comparator<E> comparator);

    boolean existsById(final String userId, final String id);

    E findByIndex(final String userId, final Integer id);

}
