package ru.tsc.gavran.tm.api.entity;

import ru.tsc.gavran.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
