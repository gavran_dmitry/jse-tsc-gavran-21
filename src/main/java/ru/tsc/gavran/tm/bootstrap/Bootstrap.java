package ru.tsc.gavran.tm.bootstrap;

import ru.tsc.gavran.tm.api.repository.*;
import ru.tsc.gavran.tm.api.service.*;
import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.command.auth.LoginCommand;
import ru.tsc.gavran.tm.command.auth.LogoutCommand;
import ru.tsc.gavran.tm.command.project.*;
import ru.tsc.gavran.tm.command.system.*;
import ru.tsc.gavran.tm.command.task.*;
import ru.tsc.gavran.tm.command.user.*;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.UnknownCommandException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.repository.*;
import ru.tsc.gavran.tm.service.*;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Predicate;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(authRepository, userService);

    public void start(String[] args) {
        displayWelcome();
        initUsers();
        initData();
        parseArgs(args);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ArgumentsDisplayCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());

        registry(new UserShowListCommand());
        registry(new UserCreateCommand());
        registry(new UserClearCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserShowByIdCommand());
        registry(new UserShowByLoginCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateByIdCommand());
        registry(new UserUpdateByLoginCommand());
    }
    public void initUsers() {
        userService.create("Test", "Test", Role.USER);
        userService.updateUserByLogin("Test", "LTest1", "FTest", "MTest", "test@gmail.com");
        userService.create("Admin", "Admin", Role.ADMIN);
        userService.updateUserByLogin("Admin", "GAVRAN", "Dmitry", "Mihailovich", "gavran@gmail.com");
    }

    private void initData() {
        projectService.create(userRepository.findByLogin("Test").getId(), "Project C", "1");
        projectService.create(userRepository.findByLogin("Test").getId(), "Project A", "2");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project B", "3");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project D", "4");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task C", "1");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task A", "2");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task B", "3");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task D", "4");
        projectService.finishByName(userRepository.findByLogin("Test").getId(), "Project C");
        projectService.startByName(userRepository.findByLogin("Admin").getId(), "Project D");
        taskService.finishByName(userRepository.findByLogin("Test").getId(), "Task C");
        taskService.startByName(userRepository.findByLogin("Admin").getId(), "Task B");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void displayAuth() {
        System.out.println("You are not authorized. " + "Enter one of the following commands: " + "\n" + "\n"
                + commandService.getCommandByName("login") + "\n"
                + commandService.getCommandByName("exit") + "\n"
                + commandService.getCommandByName("user-create") + "\n"
                + commandService.getCommandByName("about") + "\n"
                + commandService.getCommandByName("version") + "\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        String command = "";
        while (!exit.equals(command)) {
            try {
                while (!getAuthService().isAuth()) { //работа с неаутентифицированными пользователями
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit":
                                parseCommand("exit");
                                break;
                            case "login":
                                parseCommand("login");
                                break;
                            case "user-create":
                                parseCommand("user-create");
                                break;
                            case "about":
                                parseCommand("about");
                                break;
                            case "version":
                                parseCommand("version");
                                break;
                            default:
                                throw new AccessDeniedException();
                        }
                    } catch (final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    public void parseArg(final String arg) {
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}