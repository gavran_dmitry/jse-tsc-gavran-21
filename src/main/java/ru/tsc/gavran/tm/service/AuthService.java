package ru.tsc.gavran.tm.service;

import ru.tsc.gavran.tm.api.repository.IAuthRepository;
import ru.tsc.gavran.tm.api.service.IAuthService;
import ru.tsc.gavran.tm.api.service.IUserService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.empty.EmptyLoginException;
import ru.tsc.gavran.tm.exception.empty.EmptyPasswordException;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IAuthRepository authRepository;

    private final IUserService userService;

    public AuthService(IAuthRepository authRepository, IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }


    @Override
    public String getCurrentUserId() {
        final String userId = authRepository.getCurrentUserId();
        if (userId == null) throw new EmptyIdException();
        return userId;
    }

    @Override
    public void setCurrentUserId(final String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        final String currentUserId = authRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public boolean isAdmin() {
        final String userId = getCurrentUserId();
        final Role role = userService.findById(userId).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth()) throw new AccessDeniedException();
        System.out.println("See you logout, " + userService.findById(getCurrentUserId()).getFirstName());
        setCurrentUserId(null);
    }
}
