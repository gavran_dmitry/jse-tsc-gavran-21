package ru.tsc.gavran.tm.command.project;

import ru.tsc.gavran.tm.command.AbstractProjectCommand;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Removing project by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Project removedProject = serviceLocator.getProjectTaskService().removeProjectById(userId, projectId);
        if (removedProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

}