package ru.tsc.gavran.tm.command.task;

import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Display task list by project id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(userId, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 0;
        for (Task task : tasks) {
            index++;
            System.out.println(index + ". " + task.toString());
        }
    }

}