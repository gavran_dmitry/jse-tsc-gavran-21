package ru.tsc.gavran.tm.command.project;

import ru.tsc.gavran.tm.command.AbstractProjectCommand;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().startByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().startByIndex(userId, index);
        System.out.println("[OK]");
    }

}