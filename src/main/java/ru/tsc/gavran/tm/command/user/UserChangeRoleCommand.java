package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-change-role";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change the user role.";
    }

    @Override
    public void execute() {
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAdmin) throw new AccessDeniedException();
        System.out.println("ENTER USER ID: ");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER NEW ROLE: ");
        System.out.println(Arrays.toString(Role.values()));
        final String roleValue = TerminalUtil.nextLine();
        final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserService().setRole(id, role);
    }

}