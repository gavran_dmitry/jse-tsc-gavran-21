package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.repository.IProjectRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Project;

import java.util.*;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Optional<Project> project =  Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProcessException::new);
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String userId, String name, Status status) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, int index, Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}